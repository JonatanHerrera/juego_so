﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Juego_SO
{

    public partial class Preguntas : Form
    {
        int vidas = 3, avanzar;
        Random r = new Random();
        int[] array = new int[10];
        public Preguntas()
        {
            InitializeComponent();
            pregunta1.Visible = false;
            pregunta2.Visible = false;
            pregunta3.Visible = false;
            pregunta4.Visible = false;
            pregunta5.Visible = false;
            pregunta6.Visible = false;
            pregunta7.Visible = false;
            pregunta8.Visible = false;
            pregunta9.Visible = false;
            pregunta10.Visible = false;
            numeros();
        }
        public int inicio(int [] array)
        {
            avanzar = array[1];
            return avanzar;
        }
        public int[] numeros()
        {
            int num;
            for(int i=0;i<10;i++)
            {
                do
                {
                    num = r.Next(1, 10);
                    
                } while (repetido(array, num));
                array[i] = num;
            }
            
            return array;
            
        }
        public static bool repetido(int[] array, int r)
        {
            for (int i = 0; i < array.Length; i++)
                if (r == array[i])
                    return true;
            return false;
        }
       
        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (vidas > 0)
            {
                switch (array[avanzar])
                {
                    case 0:
                        pregunta1.Visible = true;
                        if (radioButton1.Checked == true)
                        {
                            if (avanzar > 9)
                            {
                                MessageBox.Show("Has ganado");
                                Menu sig = new Menu();
                                sig.Show();
                                this.Hide();
                            }
                            else
                            {
                                avanzar++;
                            }
                        }
                        else
                        {
                            vidas = vidas - 1;
                        }
                        break;
                    case 1:
                        pregunta2.Visible = true;
                        if (radioButton8.Checked == true)
                        {
                            if (avanzar > 9)
                            {
                                MessageBox.Show("Has ganado");
                                Menu sig = new Menu();
                                sig.Show();
                                this.Hide();
                            }
                            else
                            {
                                avanzar++;
                            }
                        }
                        else
                        {
                            vidas = vidas - 1;
                        }
                        break;
                    case 2:
                        pregunta3.Visible = true;
                        if (radioButton10.Checked == true)
                        {
                            if (avanzar > 9)
                            {
                                MessageBox.Show("Has ganado");
                                Menu sig = new Menu();
                                sig.Show();
                                this.Hide();
                            }
                            else
                            {
                                avanzar++;
                            }
                        }
                        else
                        {
                            vidas = vidas - 1;
                        }
                        break;
                    case 3:
                        pregunta4.Visible = true;
                        if (radioButton15.Checked == true)
                        {
                            if (avanzar > 9)
                            {
                                MessageBox.Show("Has ganado");
                                Menu sig = new Menu();
                                sig.Show();
                                this.Hide();
                            }
                            else
                            {
                                avanzar++;
                            }
                        }
                        else
                        {
                            vidas = vidas - 1;
                        }
                        break;
                    case 4:
                        pregunta5.Visible = true;
                        if (radioButton17.Checked == true)
                        {
                            if (avanzar > 9)
                            {
                                MessageBox.Show("Has ganado");
                                Menu sig = new Menu();
                                sig.Show();
                                this.Hide();
                            }
                            else
                            {
                                avanzar++;
                            }
                        }
                        else
                        {
                            vidas = vidas - 1;
                        }
                        break;
                    case 5:
                        pregunta6.Visible = true;
                        if (radioButton23.Checked == true)
                        {
                            if (avanzar > 9)
                            {
                                MessageBox.Show("Has ganado");
                                Menu sig = new Menu();
                                sig.Show();
                                this.Hide();
                            }
                            else
                            {
                                avanzar++;
                            }
                        }
                        else
                        {
                            vidas = vidas - 1;
                        }
                        break;
                    case 6:
                        pregunta7.Visible = true;
                        if (radioButton27.Checked == true)
                        {
                            if (avanzar > 9)
                            {
                                MessageBox.Show("Has ganado");
                                Menu sig = new Menu();
                                sig.Show();
                                this.Hide();
                            }
                            else
                            {
                                avanzar++;
                            }
                        }
                        else
                        {
                            vidas = vidas - 1;
                        }
                        break;
                    case 7:
                        pregunta8.Visible = true;
                        if (radioButton32.Checked == true || radioButton31.Checked == true || radioButton30.Checked == true || radioButton29.Checked == true)
                        {
                            if (avanzar > 9)
                            {
                                MessageBox.Show("Has ganado");
                                Menu sig = new Menu();
                                sig.Show();
                                this.Hide();
                            }
                            else
                            {
                                avanzar++;
                            }
                        }
                        else
                        {
                            vidas = vidas - 1;
                        }
                        break;
                    case 8:
                        pregunta9.Visible = true;
                        if (radioButton33.Checked == true)
                        {
                            if (avanzar > 9)
                            {
                                MessageBox.Show("Has ganado");
                                Menu sig = new Menu();
                                sig.Show();
                                this.Hide();
                            }
                            else
                            {
                                avanzar++;
                            }
                        }
                        else
                        {
                            vidas = vidas - 1;
                        }
                        break;
                    case 9:
                        pregunta10.Visible = true;
                        if (radioButton39.Checked == true)
                        {
                            if (avanzar > 9)
                            {
                                MessageBox.Show("Has ganado");
                                Menu sig = new Menu();
                                sig.Show();
                                this.Hide();
                            }
                            else
                            {
                                avanzar++;
                            }
                        }
                        else
                        {
                            vidas = vidas - 1;
                        }
                        break;
                    default:
                        break;
                }
            }
            else
            {
                MessageBox.Show("Has perdido");
                Menu sig = new Menu();
                sig.Show();
                this.Hide();
            }
        }
    }
}
