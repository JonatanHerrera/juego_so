﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Juego_SO
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Preguntas sig = new Preguntas();
            this.Hide();
            sig.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Instrucciones sig = new Instrucciones();
            sig.Show();
            this.Hide();
        }
    }
}
