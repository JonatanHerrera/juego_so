﻿namespace Juego_SO
{
    partial class Preguntas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pregunta1 = new System.Windows.Forms.Panel();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.pregunta2 = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.pregunta3 = new System.Windows.Forms.Panel();
            this.pregunta4 = new System.Windows.Forms.Panel();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.radioButton13 = new System.Windows.Forms.RadioButton();
            this.radioButton14 = new System.Windows.Forms.RadioButton();
            this.radioButton15 = new System.Windows.Forms.RadioButton();
            this.radioButton16 = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.radioButton9 = new System.Windows.Forms.RadioButton();
            this.radioButton10 = new System.Windows.Forms.RadioButton();
            this.radioButton11 = new System.Windows.Forms.RadioButton();
            this.radioButton12 = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.pregunta5 = new System.Windows.Forms.Panel();
            this.pregunta6 = new System.Windows.Forms.Panel();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.radioButton21 = new System.Windows.Forms.RadioButton();
            this.radioButton22 = new System.Windows.Forms.RadioButton();
            this.radioButton23 = new System.Windows.Forms.RadioButton();
            this.radioButton24 = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.radioButton17 = new System.Windows.Forms.RadioButton();
            this.radioButton18 = new System.Windows.Forms.RadioButton();
            this.radioButton19 = new System.Windows.Forms.RadioButton();
            this.radioButton20 = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.pregunta7 = new System.Windows.Forms.Panel();
            this.pregunta8 = new System.Windows.Forms.Panel();
            this.pregunta9 = new System.Windows.Forms.Panel();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.radioButton33 = new System.Windows.Forms.RadioButton();
            this.radioButton34 = new System.Windows.Forms.RadioButton();
            this.radioButton35 = new System.Windows.Forms.RadioButton();
            this.radioButton36 = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.radioButton29 = new System.Windows.Forms.RadioButton();
            this.radioButton30 = new System.Windows.Forms.RadioButton();
            this.radioButton31 = new System.Windows.Forms.RadioButton();
            this.radioButton32 = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.radioButton25 = new System.Windows.Forms.RadioButton();
            this.radioButton26 = new System.Windows.Forms.RadioButton();
            this.radioButton27 = new System.Windows.Forms.RadioButton();
            this.radioButton28 = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.pregunta10 = new System.Windows.Forms.Panel();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.radioButton37 = new System.Windows.Forms.RadioButton();
            this.radioButton38 = new System.Windows.Forms.RadioButton();
            this.radioButton39 = new System.Windows.Forms.RadioButton();
            this.radioButton40 = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.pregunta1.SuspendLayout();
            this.pregunta2.SuspendLayout();
            this.pregunta3.SuspendLayout();
            this.pregunta4.SuspendLayout();
            this.pregunta5.SuspendLayout();
            this.pregunta6.SuspendLayout();
            this.pregunta7.SuspendLayout();
            this.pregunta8.SuspendLayout();
            this.pregunta9.SuspendLayout();
            this.pregunta10.SuspendLayout();
            this.SuspendLayout();
            // 
            // pregunta1
            // 
            this.pregunta1.Controls.Add(this.radioButton4);
            this.pregunta1.Controls.Add(this.radioButton3);
            this.pregunta1.Controls.Add(this.radioButton2);
            this.pregunta1.Controls.Add(this.radioButton1);
            this.pregunta1.Controls.Add(this.label2);
            this.pregunta1.Controls.Add(this.label1);
            this.pregunta1.Location = new System.Drawing.Point(12, 12);
            this.pregunta1.Name = "pregunta1";
            this.pregunta1.Size = new System.Drawing.Size(920, 612);
            this.pregunta1.TabIndex = 0;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton4.Location = new System.Drawing.Point(34, 459);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(340, 50);
            this.radioButton4.TabIndex = 5;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Ninguna de las anteriores";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton3.Location = new System.Drawing.Point(33, 387);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(255, 50);
            this.radioButton3.TabIndex = 4;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Las dos anteriores";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton2.Location = new System.Drawing.Point(34, 312);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(400, 50);
            this.radioButton2.TabIndex = 3;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Creacion de maquinas virtuales";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton1.Location = new System.Drawing.Point(34, 240);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(551, 50);
            this.radioButton1.TabIndex = 2;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Version virtual de algun recurso tecnologico";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Chiller", 28.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(348, 56);
            this.label2.TabIndex = 1;
            this.label2.Text = "¿Que es virtualizacion?";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Chiller", 28.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(256, 56);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pregunta ........";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Chiller", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(893, 738);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(140, 47);
            this.button1.TabIndex = 1;
            this.button1.Text = "Siguiente";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pregunta2
            // 
            this.pregunta2.Controls.Add(this.textBox1);
            this.pregunta2.Controls.Add(this.radioButton8);
            this.pregunta2.Controls.Add(this.radioButton7);
            this.pregunta2.Controls.Add(this.radioButton6);
            this.pregunta2.Controls.Add(this.radioButton5);
            this.pregunta2.Controls.Add(this.label3);
            this.pregunta2.Location = new System.Drawing.Point(12, 12);
            this.pregunta2.Name = "pregunta2";
            this.pregunta2.Size = new System.Drawing.Size(917, 612);
            this.pregunta2.TabIndex = 2;
            this.pregunta2.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(22, 91);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(851, 90);
            this.textBox1.TabIndex = 6;
            this.textBox1.Text = "¿ Cual es o cuales son las operaciones importantes de la unidad de control?";
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton8.Location = new System.Drawing.Point(519, 344);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(274, 50);
            this.radioButton8.TabIndex = 5;
            this.radioButton8.TabStop = true;
            this.radioButton8.Text = "Todas las anteriores";
            this.radioButton8.UseVisualStyleBackColor = true;
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton7.Location = new System.Drawing.Point(519, 245);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(145, 50);
            this.radioButton7.TabIndex = 4;
            this.radioButton7.TabStop = true;
            this.radioButton7.Text = "Escritura";
            this.radioButton7.UseVisualStyleBackColor = true;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton6.Location = new System.Drawing.Point(91, 344);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(130, 50);
            this.radioButton6.TabIndex = 3;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "Control";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton5.Location = new System.Drawing.Point(91, 245);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(130, 50);
            this.radioButton5.TabIndex = 2;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "Lectura";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(204, 46);
            this.label3.TabIndex = 0;
            this.label3.Text = "Pregunta........";
            // 
            // pregunta3
            // 
            this.pregunta3.Controls.Add(this.pregunta4);
            this.pregunta3.Controls.Add(this.textBox2);
            this.pregunta3.Controls.Add(this.radioButton9);
            this.pregunta3.Controls.Add(this.radioButton10);
            this.pregunta3.Controls.Add(this.radioButton11);
            this.pregunta3.Controls.Add(this.radioButton12);
            this.pregunta3.Controls.Add(this.label4);
            this.pregunta3.Location = new System.Drawing.Point(12, 12);
            this.pregunta3.Name = "pregunta3";
            this.pregunta3.Size = new System.Drawing.Size(920, 615);
            this.pregunta3.TabIndex = 7;
            // 
            // pregunta4
            // 
            this.pregunta4.Controls.Add(this.textBox3);
            this.pregunta4.Controls.Add(this.radioButton13);
            this.pregunta4.Controls.Add(this.radioButton14);
            this.pregunta4.Controls.Add(this.radioButton15);
            this.pregunta4.Controls.Add(this.radioButton16);
            this.pregunta4.Controls.Add(this.label5);
            this.pregunta4.Location = new System.Drawing.Point(0, 0);
            this.pregunta4.Name = "pregunta4";
            this.pregunta4.Size = new System.Drawing.Size(920, 615);
            this.pregunta4.TabIndex = 13;
            // 
            // textBox3
            // 
            this.textBox3.Enabled = false;
            this.textBox3.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(51, 94);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(851, 90);
            this.textBox3.TabIndex = 12;
            this.textBox3.Text = "¿ Cuales son los 3 estados de vinculacion de instrucciones de la memoria ?";
            // 
            // radioButton13
            // 
            this.radioButton13.AutoSize = true;
            this.radioButton13.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton13.Location = new System.Drawing.Point(91, 493);
            this.radioButton13.Name = "radioButton13";
            this.radioButton13.Size = new System.Drawing.Size(363, 50);
            this.radioButton13.TabIndex = 11;
            this.radioButton13.TabStop = true;
            this.radioButton13.Text = "Analisi, compilacion y carga";
            this.radioButton13.UseVisualStyleBackColor = true;
            // 
            // radioButton14
            // 
            this.radioButton14.AutoSize = true;
            this.radioButton14.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton14.Location = new System.Drawing.Point(91, 403);
            this.radioButton14.Name = "radioButton14";
            this.radioButton14.Size = new System.Drawing.Size(517, 50);
            this.radioButton14.TabIndex = 10;
            this.radioButton14.TabStop = true;
            this.radioButton14.Text = "Compilacion, procesamiento y  ejecucion";
            this.radioButton14.UseVisualStyleBackColor = true;
            // 
            // radioButton15
            // 
            this.radioButton15.AutoSize = true;
            this.radioButton15.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton15.Location = new System.Drawing.Point(91, 330);
            this.radioButton15.Name = "radioButton15";
            this.radioButton15.Size = new System.Drawing.Size(403, 50);
            this.radioButton15.TabIndex = 9;
            this.radioButton15.TabStop = true;
            this.radioButton15.Text = "Compilacion, carga y ejecucion";
            this.radioButton15.UseVisualStyleBackColor = true;
            // 
            // radioButton16
            // 
            this.radioButton16.AutoSize = true;
            this.radioButton16.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton16.Location = new System.Drawing.Point(94, 248);
            this.radioButton16.Name = "radioButton16";
            this.radioButton16.Size = new System.Drawing.Size(409, 50);
            this.radioButton16.TabIndex = 8;
            this.radioButton16.TabStop = true;
            this.radioButton16.Text = "Carga, procesamiento y analisis";
            this.radioButton16.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(10, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(204, 46);
            this.label5.TabIndex = 7;
            this.label5.Text = "Pregunta........";
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(32, 94);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(851, 90);
            this.textBox2.TabIndex = 12;
            this.textBox2.Text = "¿ Como se llama l a parte del sistema operativo que administra la memoria ?";
            // 
            // radioButton9
            // 
            this.radioButton9.AutoSize = true;
            this.radioButton9.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton9.Location = new System.Drawing.Point(521, 373);
            this.radioButton9.Name = "radioButton9";
            this.radioButton9.Size = new System.Drawing.Size(340, 50);
            this.radioButton9.TabIndex = 11;
            this.radioButton9.TabStop = true;
            this.radioButton9.Text = "Ninguna de las anteriores";
            this.radioButton9.UseVisualStyleBackColor = true;
            // 
            // radioButton10
            // 
            this.radioButton10.AutoSize = true;
            this.radioButton10.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton10.Location = new System.Drawing.Point(510, 243);
            this.radioButton10.Name = "radioButton10";
            this.radioButton10.Size = new System.Drawing.Size(258, 50);
            this.radioButton10.TabIndex = 10;
            this.radioButton10.TabStop = true;
            this.radioButton10.Text = "Unidad de memoria";
            this.radioButton10.UseVisualStyleBackColor = true;
            // 
            // radioButton11
            // 
            this.radioButton11.AutoSize = true;
            this.radioButton11.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton11.Location = new System.Drawing.Point(82, 373);
            this.radioButton11.Name = "radioButton11";
            this.radioButton11.Size = new System.Drawing.Size(271, 50);
            this.radioButton11.TabIndex = 9;
            this.radioButton11.TabStop = true;
            this.radioButton11.Text = "Control de memoria";
            this.radioButton11.UseVisualStyleBackColor = true;
            // 
            // radioButton12
            // 
            this.radioButton12.AutoSize = true;
            this.radioButton12.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton12.Location = new System.Drawing.Point(78, 248);
            this.radioButton12.Name = "radioButton12";
            this.radioButton12.Size = new System.Drawing.Size(138, 50);
            this.radioButton12.TabIndex = 8;
            this.radioButton12.TabStop = true;
            this.radioButton12.Text = "Memoria";
            this.radioButton12.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(17, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(204, 46);
            this.label4.TabIndex = 7;
            this.label4.Text = "Pregunta........";
            // 
            // pregunta5
            // 
            this.pregunta5.Controls.Add(this.pregunta6);
            this.pregunta5.Controls.Add(this.textBox4);
            this.pregunta5.Controls.Add(this.radioButton17);
            this.pregunta5.Controls.Add(this.radioButton18);
            this.pregunta5.Controls.Add(this.radioButton19);
            this.pregunta5.Controls.Add(this.radioButton20);
            this.pregunta5.Controls.Add(this.label6);
            this.pregunta5.Location = new System.Drawing.Point(12, 12);
            this.pregunta5.Name = "pregunta5";
            this.pregunta5.Size = new System.Drawing.Size(930, 615);
            this.pregunta5.TabIndex = 13;
            // 
            // pregunta6
            // 
            this.pregunta6.Controls.Add(this.textBox5);
            this.pregunta6.Controls.Add(this.radioButton21);
            this.pregunta6.Controls.Add(this.radioButton22);
            this.pregunta6.Controls.Add(this.radioButton23);
            this.pregunta6.Controls.Add(this.radioButton24);
            this.pregunta6.Controls.Add(this.label7);
            this.pregunta6.Location = new System.Drawing.Point(0, 0);
            this.pregunta6.Name = "pregunta6";
            this.pregunta6.Size = new System.Drawing.Size(923, 615);
            this.pregunta6.TabIndex = 13;
            // 
            // textBox5
            // 
            this.textBox5.Enabled = false;
            this.textBox5.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.Location = new System.Drawing.Point(51, 93);
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(851, 90);
            this.textBox5.TabIndex = 12;
            this.textBox5.Text = "Caracteristica de los dispositivos de bloques";
            // 
            // radioButton21
            // 
            this.radioButton21.AutoSize = true;
            this.radioButton21.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton21.Location = new System.Drawing.Point(40, 437);
            this.radioButton21.Name = "radioButton21";
            this.radioButton21.Size = new System.Drawing.Size(921, 50);
            this.radioButton21.TabIndex = 11;
            this.radioButton21.TabStop = true;
            this.radioButton21.Text = "Los tamaños de los bloques van desde los 128 bytes hasta los 1024 bytes";
            this.radioButton21.UseVisualStyleBackColor = true;
            // 
            // radioButton22
            // 
            this.radioButton22.AutoSize = true;
            this.radioButton22.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton22.Location = new System.Drawing.Point(40, 348);
            this.radioButton22.Name = "radioButton22";
            this.radioButton22.Size = new System.Drawing.Size(667, 50);
            this.radioButton22.TabIndex = 10;
            this.radioButton22.TabStop = true;
            this.radioButton22.Text = "La informacion se alamcena en bloques de tamaño fijo";
            this.radioButton22.UseVisualStyleBackColor = true;
            // 
            // radioButton23
            // 
            this.radioButton23.AutoSize = true;
            this.radioButton23.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton23.Location = new System.Drawing.Point(40, 515);
            this.radioButton23.Name = "radioButton23";
            this.radioButton23.Size = new System.Drawing.Size(274, 50);
            this.radioButton23.TabIndex = 9;
            this.radioButton23.TabStop = true;
            this.radioButton23.Text = "Todas las anteriores";
            this.radioButton23.UseVisualStyleBackColor = true;
            // 
            // radioButton24
            // 
            this.radioButton24.AutoSize = true;
            this.radioButton24.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton24.Location = new System.Drawing.Point(40, 274);
            this.radioButton24.Name = "radioButton24";
            this.radioButton24.Size = new System.Drawing.Size(482, 50);
            this.radioButton24.TabIndex = 8;
            this.radioButton24.TabStop = true;
            this.radioButton24.Text = "Cada bloque tiene su propia direccion";
            this.radioButton24.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(10, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(204, 46);
            this.label7.TabIndex = 7;
            this.label7.Text = "Pregunta........";
            // 
            // textBox4
            // 
            this.textBox4.Enabled = false;
            this.textBox4.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(51, 96);
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(851, 90);
            this.textBox4.TabIndex = 12;
            this.textBox4.Text = "¿ Cuales son las clasificaciones de operaciones de entrada y salida";
            // 
            // radioButton17
            // 
            this.radioButton17.AutoSize = true;
            this.radioButton17.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton17.Location = new System.Drawing.Point(522, 370);
            this.radioButton17.Name = "radioButton17";
            this.radioButton17.Size = new System.Drawing.Size(286, 50);
            this.radioButton17.TabIndex = 11;
            this.radioButton17.TabStop = true;
            this.radioButton17.Text = "Bloques y caracteres";
            this.radioButton17.UseVisualStyleBackColor = true;
            // 
            // radioButton18
            // 
            this.radioButton18.AutoSize = true;
            this.radioButton18.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton18.Location = new System.Drawing.Point(520, 240);
            this.radioButton18.Name = "radioButton18";
            this.radioButton18.Size = new System.Drawing.Size(285, 50);
            this.radioButton18.TabIndex = 10;
            this.radioButton18.TabStop = true;
            this.radioButton18.Text = "Caracteres y analisis";
            this.radioButton18.UseVisualStyleBackColor = true;
            // 
            // radioButton19
            // 
            this.radioButton19.AutoSize = true;
            this.radioButton19.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton19.Location = new System.Drawing.Point(94, 370);
            this.radioButton19.Name = "radioButton19";
            this.radioButton19.Size = new System.Drawing.Size(277, 50);
            this.radioButton19.TabIndex = 9;
            this.radioButton19.TabStop = true;
            this.radioButton19.Text = "Bloques y ejecucion";
            this.radioButton19.UseVisualStyleBackColor = true;
            // 
            // radioButton20
            // 
            this.radioButton20.AutoSize = true;
            this.radioButton20.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton20.Location = new System.Drawing.Point(97, 240);
            this.radioButton20.Name = "radioButton20";
            this.radioButton20.Size = new System.Drawing.Size(341, 50);
            this.radioButton20.TabIndex = 8;
            this.radioButton20.TabStop = true;
            this.radioButton20.Text = "Administracio y ejecucion";
            this.radioButton20.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(10, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(204, 46);
            this.label6.TabIndex = 7;
            this.label6.Text = "Pregunta........";
            // 
            // pregunta7
            // 
            this.pregunta7.Controls.Add(this.pregunta8);
            this.pregunta7.Controls.Add(this.textBox6);
            this.pregunta7.Controls.Add(this.radioButton25);
            this.pregunta7.Controls.Add(this.radioButton26);
            this.pregunta7.Controls.Add(this.radioButton27);
            this.pregunta7.Controls.Add(this.radioButton28);
            this.pregunta7.Controls.Add(this.label8);
            this.pregunta7.Location = new System.Drawing.Point(12, 12);
            this.pregunta7.Name = "pregunta7";
            this.pregunta7.Size = new System.Drawing.Size(930, 616);
            this.pregunta7.TabIndex = 13;
            // 
            // pregunta8
            // 
            this.pregunta8.Controls.Add(this.pregunta9);
            this.pregunta8.Controls.Add(this.textBox7);
            this.pregunta8.Controls.Add(this.radioButton29);
            this.pregunta8.Controls.Add(this.radioButton30);
            this.pregunta8.Controls.Add(this.radioButton31);
            this.pregunta8.Controls.Add(this.radioButton32);
            this.pregunta8.Controls.Add(this.label9);
            this.pregunta8.Location = new System.Drawing.Point(0, 0);
            this.pregunta8.Name = "pregunta8";
            this.pregunta8.Size = new System.Drawing.Size(930, 616);
            this.pregunta8.TabIndex = 19;
            // 
            // pregunta9
            // 
            this.pregunta9.Controls.Add(this.textBox8);
            this.pregunta9.Controls.Add(this.radioButton33);
            this.pregunta9.Controls.Add(this.radioButton34);
            this.pregunta9.Controls.Add(this.radioButton35);
            this.pregunta9.Controls.Add(this.radioButton36);
            this.pregunta9.Controls.Add(this.label10);
            this.pregunta9.Location = new System.Drawing.Point(3, 0);
            this.pregunta9.Name = "pregunta9";
            this.pregunta9.Size = new System.Drawing.Size(927, 617);
            this.pregunta9.TabIndex = 19;
            this.pregunta9.Visible = false;
            // 
            // textBox8
            // 
            this.textBox8.Enabled = false;
            this.textBox8.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox8.Location = new System.Drawing.Point(51, 92);
            this.textBox8.Multiline = true;
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(851, 90);
            this.textBox8.TabIndex = 18;
            this.textBox8.Text = "Selecciona un criterio principal para la seleccion de software de virtualizacion";
            // 
            // radioButton33
            // 
            this.radioButton33.AutoSize = true;
            this.radioButton33.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton33.Location = new System.Drawing.Point(520, 409);
            this.radioButton33.Name = "radioButton33";
            this.radioButton33.Size = new System.Drawing.Size(51, 50);
            this.radioButton33.TabIndex = 17;
            this.radioButton33.TabStop = true;
            this.radioButton33.Text = ".";
            this.radioButton33.UseVisualStyleBackColor = true;
            // 
            // radioButton34
            // 
            this.radioButton34.AutoSize = true;
            this.radioButton34.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton34.Location = new System.Drawing.Point(518, 280);
            this.radioButton34.Name = "radioButton34";
            this.radioButton34.Size = new System.Drawing.Size(153, 50);
            this.radioButton34.TabIndex = 16;
            this.radioButton34.TabStop = true;
            this.radioButton34.Text = "Usabilidad";
            this.radioButton34.UseVisualStyleBackColor = true;
            // 
            // radioButton35
            // 
            this.radioButton35.AutoSize = true;
            this.radioButton35.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton35.Location = new System.Drawing.Point(92, 393);
            this.radioButton35.Name = "radioButton35";
            this.radioButton35.Size = new System.Drawing.Size(183, 50);
            this.radioButton35.TabIndex = 15;
            this.radioButton35.TabStop = true;
            this.radioButton35.Text = "Rendimiento";
            this.radioButton35.UseVisualStyleBackColor = true;
            // 
            // radioButton36
            // 
            this.radioButton36.AutoSize = true;
            this.radioButton36.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton36.Location = new System.Drawing.Point(92, 280);
            this.radioButton36.Name = "radioButton36";
            this.radioButton36.Size = new System.Drawing.Size(110, 50);
            this.radioButton36.TabIndex = 14;
            this.radioButton36.TabStop = true;
            this.radioButton36.Text = "Costo";
            this.radioButton36.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(10, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(204, 46);
            this.label10.TabIndex = 13;
            this.label10.Text = "Pregunta........";
            // 
            // textBox7
            // 
            this.textBox7.Enabled = false;
            this.textBox7.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.Location = new System.Drawing.Point(54, 94);
            this.textBox7.Multiline = true;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(851, 90);
            this.textBox7.TabIndex = 18;
            this.textBox7.Text = "Selecciona la opcion donde se muestre una ventaja y una desventaja de la virtuali" +
    "zacion";
            // 
            // radioButton29
            // 
            this.radioButton29.AutoSize = true;
            this.radioButton29.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton29.Location = new System.Drawing.Point(91, 443);
            this.radioButton29.Name = "radioButton29";
            this.radioButton29.Size = new System.Drawing.Size(734, 50);
            this.radioButton29.TabIndex = 17;
            this.radioButton29.TabStop = true;
            this.radioButton29.Text = "Seguridad y limitacion del hardware de ls maquinas virtuales";
            this.radioButton29.UseVisualStyleBackColor = true;
            // 
            // radioButton30
            // 
            this.radioButton30.AutoSize = true;
            this.radioButton30.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton30.Location = new System.Drawing.Point(97, 373);
            this.radioButton30.Name = "radioButton30";
            this.radioButton30.Size = new System.Drawing.Size(517, 50);
            this.radioButton30.TabIndex = 16;
            this.radioButton30.TabStop = true;
            this.radioButton30.Text = "Ahorro de espacio y rendimiento inferior";
            this.radioButton30.UseVisualStyleBackColor = true;
            // 
            // radioButton31
            // 
            this.radioButton31.AutoSize = true;
            this.radioButton31.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton31.Location = new System.Drawing.Point(96, 532);
            this.radioButton31.Name = "radioButton31";
            this.radioButton31.Size = new System.Drawing.Size(725, 50);
            this.radioButton31.TabIndex = 15;
            this.radioButton31.TabStop = true;
            this.radioButton31.Text = "Escabilidad y muchos sistemas dependen de un solo equipo";
            this.radioButton31.UseVisualStyleBackColor = true;
            // 
            // radioButton32
            // 
            this.radioButton32.AutoSize = true;
            this.radioButton32.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton32.Location = new System.Drawing.Point(95, 275);
            this.radioButton32.Name = "radioButton32";
            this.radioButton32.Size = new System.Drawing.Size(512, 50);
            this.radioButton32.TabIndex = 14;
            this.radioButton32.TabStop = true;
            this.radioButton32.Text = "Seguridad y Aumento de costos iniciales";
            this.radioButton32.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(13, 13);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(204, 46);
            this.label9.TabIndex = 13;
            this.label9.Text = "Pregunta........";
            // 
            // textBox6
            // 
            this.textBox6.Enabled = false;
            this.textBox6.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(54, 99);
            this.textBox6.Multiline = true;
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(851, 90);
            this.textBox6.TabIndex = 18;
            this.textBox6.Text = "Caracteristica de los dispositivos de caracteres";
            // 
            // radioButton25
            // 
            this.radioButton25.AutoSize = true;
            this.radioButton25.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton25.Location = new System.Drawing.Point(91, 443);
            this.radioButton25.Name = "radioButton25";
            this.radioButton25.Size = new System.Drawing.Size(415, 50);
            this.radioButton25.TabIndex = 17;
            this.radioButton25.TabStop = true;
            this.radioButton25.Text = "No tiene operacion de busqueda";
            this.radioButton25.UseVisualStyleBackColor = true;
            // 
            // radioButton26
            // 
            this.radioButton26.AutoSize = true;
            this.radioButton26.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton26.Location = new System.Drawing.Point(94, 368);
            this.radioButton26.Name = "radioButton26";
            this.radioButton26.Size = new System.Drawing.Size(287, 50);
            this.radioButton26.TabIndex = 16;
            this.radioButton26.TabStop = true;
            this.radioButton26.Text = "No utilza direcciones";
            this.radioButton26.UseVisualStyleBackColor = true;
            // 
            // radioButton27
            // 
            this.radioButton27.AutoSize = true;
            this.radioButton27.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton27.Location = new System.Drawing.Point(97, 532);
            this.radioButton27.Name = "radioButton27";
            this.radioButton27.Size = new System.Drawing.Size(274, 50);
            this.radioButton27.TabIndex = 15;
            this.radioButton27.TabStop = true;
            this.radioButton27.Text = "Todas las anteriores";
            this.radioButton27.UseVisualStyleBackColor = true;
            // 
            // radioButton28
            // 
            this.radioButton28.AutoSize = true;
            this.radioButton28.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton28.Location = new System.Drawing.Point(95, 280);
            this.radioButton28.Name = "radioButton28";
            this.radioButton28.Size = new System.Drawing.Size(701, 50);
            this.radioButton28.TabIndex = 14;
            this.radioButton28.TabStop = true;
            this.radioButton28.Text = "La informacion se transfiere como un flujo de caracteres";
            this.radioButton28.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(13, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(204, 46);
            this.label8.TabIndex = 13;
            this.label8.Text = "Pregunta........";
            // 
            // pregunta10
            // 
            this.pregunta10.Controls.Add(this.textBox9);
            this.pregunta10.Controls.Add(this.radioButton37);
            this.pregunta10.Controls.Add(this.radioButton38);
            this.pregunta10.Controls.Add(this.radioButton39);
            this.pregunta10.Controls.Add(this.radioButton40);
            this.pregunta10.Controls.Add(this.label11);
            this.pregunta10.Location = new System.Drawing.Point(12, 12);
            this.pregunta10.Name = "pregunta10";
            this.pregunta10.Size = new System.Drawing.Size(1021, 703);
            this.pregunta10.TabIndex = 19;
            // 
            // textBox9
            // 
            this.textBox9.Enabled = false;
            this.textBox9.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox9.Location = new System.Drawing.Point(54, 94);
            this.textBox9.Multiline = true;
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(851, 90);
            this.textBox9.TabIndex = 18;
            this.textBox9.Text = "Cuales son las colas que son usadas para planificar los recursos/memoria del S.O." +
    "";
            // 
            // radioButton37
            // 
            this.radioButton37.AutoSize = true;
            this.radioButton37.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton37.Location = new System.Drawing.Point(95, 459);
            this.radioButton37.Name = "radioButton37";
            this.radioButton37.Size = new System.Drawing.Size(409, 50);
            this.radioButton37.TabIndex = 17;
            this.radioButton37.TabStop = true;
            this.radioButton37.Text = "De trabajo, memoria y procesos";
            this.radioButton37.UseVisualStyleBackColor = true;
            // 
            // radioButton38
            // 
            this.radioButton38.AutoSize = true;
            this.radioButton38.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton38.Location = new System.Drawing.Point(91, 304);
            this.radioButton38.Name = "radioButton38";
            this.radioButton38.Size = new System.Drawing.Size(386, 50);
            this.radioButton38.TabIndex = 16;
            this.radioButton38.TabStop = true;
            this.radioButton38.Text = "Memoria, procesos, ejecucion";
            this.radioButton38.UseVisualStyleBackColor = true;
            // 
            // radioButton39
            // 
            this.radioButton39.AutoSize = true;
            this.radioButton39.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton39.Location = new System.Drawing.Point(96, 549);
            this.radioButton39.Name = "radioButton39";
            this.radioButton39.Size = new System.Drawing.Size(444, 50);
            this.radioButton39.TabIndex = 15;
            this.radioButton39.TabStop = true;
            this.radioButton39.Text = "De trabajo, procesos y dispositivos";
            this.radioButton39.UseVisualStyleBackColor = true;
            // 
            // radioButton40
            // 
            this.radioButton40.AutoSize = true;
            this.radioButton40.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton40.Location = new System.Drawing.Point(94, 381);
            this.radioButton40.Name = "radioButton40";
            this.radioButton40.Size = new System.Drawing.Size(402, 50);
            this.radioButton40.TabIndex = 14;
            this.radioButton40.TabStop = true;
            this.radioButton40.Text = "Dispositivos, memoria y analisis";
            this.radioButton40.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Chiller", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(13, 13);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(204, 46);
            this.label11.TabIndex = 13;
            this.label11.Text = "Pregunta........";
            // 
            // Preguntas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1075, 805);
            this.Controls.Add(this.pregunta10);
            this.Controls.Add(this.pregunta7);
            this.Controls.Add(this.pregunta5);
            this.Controls.Add(this.pregunta3);
            this.Controls.Add(this.pregunta2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pregunta1);
            this.Name = "Preguntas";
            this.Text = "Preguntas";
            this.pregunta1.ResumeLayout(false);
            this.pregunta1.PerformLayout();
            this.pregunta2.ResumeLayout(false);
            this.pregunta2.PerformLayout();
            this.pregunta3.ResumeLayout(false);
            this.pregunta3.PerformLayout();
            this.pregunta4.ResumeLayout(false);
            this.pregunta4.PerformLayout();
            this.pregunta5.ResumeLayout(false);
            this.pregunta5.PerformLayout();
            this.pregunta6.ResumeLayout(false);
            this.pregunta6.PerformLayout();
            this.pregunta7.ResumeLayout(false);
            this.pregunta7.PerformLayout();
            this.pregunta8.ResumeLayout(false);
            this.pregunta8.PerformLayout();
            this.pregunta9.ResumeLayout(false);
            this.pregunta9.PerformLayout();
            this.pregunta10.ResumeLayout(false);
            this.pregunta10.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pregunta1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel pregunta2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.Panel pregunta3;
        private System.Windows.Forms.Panel pregunta4;
        private System.Windows.Forms.Panel pregunta5;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.RadioButton radioButton13;
        private System.Windows.Forms.RadioButton radioButton14;
        private System.Windows.Forms.RadioButton radioButton15;
        private System.Windows.Forms.RadioButton radioButton16;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.RadioButton radioButton9;
        private System.Windows.Forms.RadioButton radioButton10;
        private System.Windows.Forms.RadioButton radioButton11;
        private System.Windows.Forms.RadioButton radioButton12;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel pregunta6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.RadioButton radioButton21;
        private System.Windows.Forms.RadioButton radioButton22;
        private System.Windows.Forms.RadioButton radioButton23;
        private System.Windows.Forms.RadioButton radioButton24;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.RadioButton radioButton17;
        private System.Windows.Forms.RadioButton radioButton18;
        private System.Windows.Forms.RadioButton radioButton19;
        private System.Windows.Forms.RadioButton radioButton20;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel pregunta7;
        private System.Windows.Forms.Panel pregunta8;
        private System.Windows.Forms.Panel pregunta9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.RadioButton radioButton33;
        private System.Windows.Forms.RadioButton radioButton34;
        private System.Windows.Forms.RadioButton radioButton35;
        private System.Windows.Forms.RadioButton radioButton36;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.RadioButton radioButton29;
        private System.Windows.Forms.RadioButton radioButton30;
        private System.Windows.Forms.RadioButton radioButton31;
        private System.Windows.Forms.RadioButton radioButton32;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.RadioButton radioButton25;
        private System.Windows.Forms.RadioButton radioButton26;
        private System.Windows.Forms.RadioButton radioButton27;
        private System.Windows.Forms.RadioButton radioButton28;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel pregunta10;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.RadioButton radioButton37;
        private System.Windows.Forms.RadioButton radioButton38;
        private System.Windows.Forms.RadioButton radioButton39;
        private System.Windows.Forms.RadioButton radioButton40;
        private System.Windows.Forms.Label label11;
    }
}